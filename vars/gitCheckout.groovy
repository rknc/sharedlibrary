def gitImport() {
   git credentialsId: '850eec72-c779-4865-8dfd-a850afcf1464', url: 'https://akashmarla@bitbucket.org/akashmarla/test-push-pull.git'
   filename = 'C:\\Program Files (x86)\\Jenkins\\workspace\\Test\\gitDetails.properties'
    //property files
    
    Properties props = new Properties()
    File propsFile = new File(filename)
    props.load(propsFile.newDataInputStream())
    
    def r=props.getProperty('version')
    
    r.split('.')
    count=r[0].toInteger()+1
   
    //r[0]=count.toString()
    
    def str=count.toString()+r[1]+r[2]+r[3]+r[4]
    
    props.setProperty('version', str)
    props.store(propsFile.newWriter(), null)
    
}
node { 
    stage('Git'){
        gitImport()
    }
}
